/****************************************************************************
 *                                                                          *
 *      Programa feito para comparar diferentes casos do Merge Sort	    *
 *  Desenvolvido para a cadeira de Análise e Projeto de Algoritmos - UFPB   *
 *                  Lencionada pelo Prof. Bruno Petrato                     *
 *                                                                          *
 ****************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<time.h>

//Define o tamanho do array
#define TAMANHO 40

//Função para exibir todos os elementos do array
void PrintArray(int A[], int n) {
    for(int i = 0; i < n; i++) {
        printf("%d ", A[i]);
    }
    
    printf("\n");
}

//*****Implementação do algoritmo do Merge Sort em C*****

//Função que cria dois arrays temporarios e depois aplica
//seus valores no array principal.
void Merge(int A[], int comeco, int meio, int fim) {
	//Definindo o tamanho dos dois arrays temporários
	//dividindo o array principal no meio
	int size_l = meio - comeco + 1;
	int size_r = fim - meio;

	//Criando os dois arrays temporários
	int L[size_l], R[size_r];
	int i = 0, j = 0;

	//Preenchendo os arrays temporários com os valores
	//do array principal 
	for (i = 0; i < size_l; i++) L[i] = A[comeco + i];
	for (j = 0; j < size_r; j++) R[j] = A[meio + 1 + j];

	//Marcando o final do array com um número bem grande
	L[i] = 999999;
	R[j] = 999999;

	//Loop que percorre o array principal e ordenando
	//dependendo do array temporário que tem o menor
	//elemento.
	//Sendo que o array temporário foi ordenado por
	//causa da recursão do algoritmo.
	i = 0; j = 0;
	for (int k = comeco; k <= fim; k++) {
		if (L[i] <= R[j]) 
			A[k] = L[i++];
		else 
			A[k] = R[j++];
	}
}

//Função que chama o Merge Sort de fato
//É onde está implementado a chamada da recursão
void MergeSort(int A[], int comeco, int fim) {
	if (comeco < fim) {
		int meio = (comeco + fim)/2;
		MergeSort(A, comeco, meio);
		MergeSort(A, meio+1, fim);
		Merge(A, comeco, meio, fim);
	}	
}

//Preenche o array de forma já ordenada
void CasoOrdenado(int A[], int n) {
    for(int i = 0; i < n; i++) {
        A[i] = i;
    }
}

//Preenche o array ordenado ao contrário
void CasoDecrescente(int A[], int n) {
    int j = n;
    for(int i = 0; i < n; i++) {
        A[i] = j - i;
    }
}

//Preenche o array com valores aleatórios
void CasoAleatorio(int A[], int n) {
    for(int i = 0; i < n; i++) {
        A[i] = rand() % n;
    }
}

int main() {
    int opcao, elementos[TAMANHO];
    //As variáveis do tipo clock_t salvam o valor do relógio do CPU
    //isso será usado para calcular o tempo de execução do algoritmo
    //O resultado é retornado em ticks de clock por segundo
    clock_t start, end;
    
    while(1) {
        printf("Escolha uma das opções:\n 1-) Melhor caso.\n 2-) Pior caso.\n 3-) Caso aleatório.\n 0-) Encerrar o programa\n");
        scanf("%d", &opcao);
        if(opcao == 0) break;
        puts("");
        
        switch(opcao) {
            case 1:
                CasoOrdenado(elementos, TAMANHO);
                puts("O caso ordenado é:");
                PrintArray(elementos, TAMANHO);
                
                start = clock();
                MergeSort(elementos, 0, TAMANHO);
                end = clock();
                
                puts("Resultado final:");
                PrintArray(elementos, TAMANHO);
                printf("Tempo gasto para organizar o array: %f ticks/s\n\n", (double)(end - start) / CLOCKS_PER_SEC);
                break;
                
            case 2:
                CasoDecrescente(elementos, TAMANHO);
                puts("O caso decrescete é:");
                PrintArray(elementos, TAMANHO);
                
                start = clock();
                MergeSort(elementos, 0, TAMANHO);
                end = clock();
                
                puts("Resultado final:");
                PrintArray(elementos, TAMANHO);
                printf("Tempo gasto para organizar o array: %f ticks/s\n\n", (double)(end - start) / CLOCKS_PER_SEC);
                break;
            
            case 3:
                CasoAleatorio(elementos, TAMANHO);
                puts("O array gerado foi:");
                PrintArray(elementos, TAMANHO);
                
                start = clock();
                MergeSort(elementos, 0, TAMANHO);
                end = clock();
                
                puts("Resultado final:");
                PrintArray(elementos, TAMANHO);
                printf("Tempo gasto para organizar o array: %f ticks/s\n\n", (double)(end - start) / CLOCKS_PER_SEC); 
                break;
                
            default:
                puts("Entrada inválida.");
                break;
        }
    }
    
    return 0;
}
