/****************************************************************************
 *                                                                          *
 *      Programa feito para comparar diferentes casos do Quick Sort    	    *
 *  Desenvolvido para a cadeira de Análise e Projeto de Algoritmos - UFPB   *
 *                  Lencionada pelo Prof. Bruno Petrato                     *
 *                                                                          *
 ****************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<time.h>

//Define o tamanho do array
#define TAMANHO 40

//Função para exibir todos os elementos do array
void PrintArray(int A[], int n) {
    for(int i = 0; i < n; i++) {
        printf("%d ", A[i]);
    }
    
    printf("\n");
}

//Função que particiona o array
int Partition(int A[], int comeco, int fim) {
	int pivot = A[fim];
	int i = comeco - 1;

	for (int j = comeco; j < fim; j++) {
		if (A[j] < pivot) {
			i++;
			int tmp = A[i];
			A[i] = A[j];
			A[j] = tmp;
		}
	}

	if (A[fim] < A[i + 1]) {
		int tmp = A[i + 1];
		A[i + 1] = A[fim];
		A[fim] = tmp;
	}

	return i + 1;
}

//Função que chama o Quick Sort de fato
//É onde está implementado a chamada da recursão
void QuickSort(int A[], int comeco, int fim) {
	if (comeco < fim) {
		int p = Partition(A, comeco, fim);
		QuickSort(A, comeco, p - 1);
		QuickSort(A, p + 1, fim);
	}	
}

//Preenche o array de forma já ordenada
void CasoOrdenado(int A[], int n) {
    for(int i = 0; i < n; i++) {
        A[i] = i;
    }
}

//Preenche o array ordenado ao contrário
void CasoDecrescente(int A[], int n) {
    int j = n;
    for(int i = 0; i < n; i++) {
        A[i] = j - i;
    }
}

//Preenche o array com valores aleatórios
void CasoAleatorio(int A[], int n) {
    for(int i = 0; i < n; i++) {
        A[i] = rand() % n;
    }
}

int main() {
    int opcao, elementos[TAMANHO];
    //As variáveis do tipo clock_t salvam o valor do relógio do CPU
    //isso será usado para calcular o tempo de execução do algoritmo
    //O resultado é retornado em ticks de clock por segundo
    clock_t start, end;
    
    while(1) {
        printf("Escolha uma das opções:\n 1-) Melhor caso.\n 2-) Pior caso.\n 3-) Caso aleatório.\n 0-) Encerrar o programa\n");
        scanf("%d", &opcao);
        if(opcao == 0) break;
        puts("");
        
        switch(opcao) {
            case 1:
                CasoOrdenado(elementos, TAMANHO);
                puts("O caso ordenado é:");
                PrintArray(elementos, TAMANHO);
                
                start = clock();
                QuickSort(elementos, 0, TAMANHO);
                end = clock();
                
                puts("Resultado final:");
                PrintArray(elementos, TAMANHO);
                printf("Tempo gasto para organizar o array: %f ticks/s\n\n", (double)(end - start) / CLOCKS_PER_SEC);
                break;
                
            case 2:
                CasoDecrescente(elementos, TAMANHO);
                puts("O caso decrescete é:");
                PrintArray(elementos, TAMANHO);
                
                start = clock();
                QuickSort(elementos, 0, TAMANHO);
                end = clock();
                
                puts("Resultado final:");
                PrintArray(elementos, TAMANHO);
                printf("Tempo gasto para organizar o array: %f ticks/s\n\n", (double)(end - start) / CLOCKS_PER_SEC);
                break;
            
            case 3:
                CasoAleatorio(elementos, TAMANHO);
                puts("O array gerado foi:");
                PrintArray(elementos, TAMANHO);
                
                start = clock();
                QuickSort(elementos, 0, TAMANHO);
                end = clock();
                
                puts("Resultado final:");
                PrintArray(elementos, TAMANHO);
                printf("Tempo gasto para organizar o array: %f ticks/s\n\n", (double)(end - start) / CLOCKS_PER_SEC); 
                break;
                
            default:
                puts("Entrada inválida.");
                break;
        }
    }
    
    return 0;
}
