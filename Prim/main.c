/************************************
 *       ALGORITMO DE PRIM	        *
 *   Feito em C para a cadeira de   *
 *  An�lise e Projeto de Algoritmos *
 *         Prof. Bruno Bruck        *
 *                                  *
 * CI-UFPB Feito por Gabriel Ferraz *
 ************************************/
 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>

int chave_minima(int chave[], bool conjunto_MST[], int num_vertices) {
	int min_index = -1, min = INT_MAX;

	for(int i = 0; i < num_vertices; i++) {
		if(conjunto_MST[i] == false && chave[i] < min) {
			min = chave[i];
			min_index = i;
		}
	}

	return min_index;
}

void Prim(int **grafo, int num_vertices) {
	int pai[num_vertices];
	int chave[num_vertices];
	bool conjunto_MST[num_vertices];

	for(int i = 0; i < num_vertices; i++) {
		chave[i] = INT_MAX;
		conjunto_MST[i] = false;
	}

	chave[0] = 0;
	pai[0] = -1;

	for(int i = 0; i < num_vertices-1; i++) {
		int indexMin = chave_minima(chave, conjunto_MST, num_vertices);
		conjunto_MST[indexMin] = true;

		for(int j = 0; j < num_vertices; j++) {
			if(grafo[indexMin][j] > 0 && conjunto_MST[j] == false && grafo[indexMin][j] < chave[j]) {
				pai[j] = indexMin;
				chave[j] = grafo[indexMin][j];
			}
		}
	}

	int custo_total = 0;
	puts("Arvore minima encontrada:");
	for(int i = 1; i < num_vertices; i++) {
		printf("%d --(%d)--> %d\n", pai[i], chave[i], i);
		custo_total += chave[i];
	}
	printf("Custo total: %d\n", custo_total);
}

void fileToGrafo(FILE *f) {
	int v;
	int **grafo;
	fscanf(f, "%d", &v);

	grafo = (int **)malloc(v * sizeof(int *));
	for(int i = 0; i < v; i++) grafo[i] = (int *)malloc(v * sizeof(int));

	int offset = 1, index = 0;
	for(int i = 0; i < v; i++) {
		for(int j = offset; j < v; j++) {
			fscanf(f, "%d", &(grafo[i][j]));
			index++;
		}
		offset++;
	}
	puts("");

	puts("O grafo criado foi:");
	for(int i = 0; i < v; i++) {
		for(int j = 0; j < v; j++) {
			if(grafo[i][j] != 0)
				printf("%d --(%d)--> %d\n", i, grafo[i][j], j);
		}
	}
	Prim(grafo, v);
	free(grafo);
}

int main() {
	FILE *fp;
	int i;
	puts("*****ALGORITMO DE PRIM*****");

	while(1) {
		printf("Selecione um arquivo de teste:\n1-)dij10.txt\n2-)dij20.txt\n3-)dij40.txt\n4-)dij50.txt\n5-)Sair\n");
		scanf("%d", &i);

		switch(i) {
			case 1:
				fp = fopen("test_files/dij10.txt", "r");
				break;
			case 2:
				fp = fopen("test_files/dij20.txt", "r");
				break;
			case 3:
				fp = fopen("test_files/dij40.txt", "r");
				break;
			case 4:
				fp = fopen("test_files/dij50.txt", "r");
				break;
			case 5:
				return 0;
			default:
				puts("Entrada invalida.");
				break;
		}	

		fileToGrafo(fp);
	}
}
