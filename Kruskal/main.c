/************************************
 *       ALGORITMO DE KRUSKAL       *
 *   Feito em C para a cadeira de   *
 *  Análise e Projeto de Algoritmos *
 *         Prof. Bruno Bruck        *
 *                                  *
 * CI-UFPB Feito por Gabriel Ferraz *
 ************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Aresta {
	int src, dest, peso;
} tAresta;

typedef struct Grafo {
	int num_vertices, num_arestas;
	tAresta *arestas;
} tGrafo;

typedef struct Subconjunto {
	int parente,rank;
} tSubconjunto;

tGrafo* criaGrafo(int V, int A) {
	tGrafo* grafo = (tGrafo*) malloc(sizeof(tGrafo));
	grafo->num_vertices = V;
	grafo->num_arestas = A;
	grafo->arestas = (tAresta*) malloc(grafo->num_arestas * sizeof(tAresta));
	return grafo;
}

int encontra(tSubconjunto* subconjuntos, int i) {
	if(subconjuntos[i].parente != i) 
		subconjuntos[i].parente = encontra(subconjuntos, subconjuntos[i].parente);

	return subconjuntos[i].parente;
}

int unir(tSubconjunto* subconjuntos, int x1, int x2) {
	int raiz_x1 = encontra(subconjuntos, x1);
	int raiz_x2 = encontra(subconjuntos, x2);
	
	if(subconjuntos[raiz_x1].rank < subconjuntos[raiz_x2].rank)
		subconjuntos[raiz_x1].parente = raiz_x2;
	else if(subconjuntos[raiz_x1].rank > subconjuntos[raiz_x2].rank)
		subconjuntos[raiz_x2].parente = raiz_x1;
	else {
		subconjuntos[raiz_x2].parente = raiz_x1;
		subconjuntos[raiz_x1].rank++;
	}
}

int comparaArestas(const void* a, const void* b){
	tAresta* a1 = (tAresta*) a;
	tAresta* b1 = (tAresta*) b;
	return a1->peso > b1->peso;
}

void Kruskal(tGrafo* grafo) {
	int V = grafo->num_vertices;
	tAresta resultado[V];
	int e = 0, i = 0;

	qsort(grafo->arestas, grafo->num_arestas, sizeof(grafo->arestas[0]), comparaArestas);

	tSubconjunto* subconjuntos = (tSubconjunto*) malloc(grafo->num_vertices * sizeof(tSubconjunto));
	for(int v = 0; v < V; v++) {
		subconjuntos[v].parente = v;
		subconjuntos[v].rank = 0;
	}

	while (e < V-1) {
		tAresta prox_aresta = grafo->arestas[i++];

		int x1 = encontra(subconjuntos, prox_aresta.src);
		int x2 = encontra(subconjuntos, prox_aresta.dest);

		if (x1 != x2) {
			resultado[e++] = prox_aresta;
			unir(subconjuntos, x1, x2);
		}
	}

	puts("Árvore minima gerada:");
	int custoTotal = 0;
	for(i = 0; i < e; i++) {
		printf("%d --(%d)--> %d\n", resultado[i].src, resultado[i].peso, resultado[i].dest);
		custoTotal += resultado[i].peso;
	}
	printf("Custo total: %d\n", custoTotal);
}

void fileToGrafo(tGrafo *grafo, FILE *f) {
	int v, a = 0;
	fscanf(f, "%d", &v);
	for(int i = v-1; i > 0; i--) {
		a += i;
	}

	grafo = criaGrafo(v, a);
	int offset = 1, index = 0;
	for(int i = 0; i < v; i++) {
		for(int j = offset; j < v; j++) {
			grafo->arestas[index].src = i;
			grafo->arestas[index].dest = j;
			fscanf(f, "%d", &(grafo->arestas[index].peso));
			index++;
		}
		offset++;
	}
	puts("");

	puts("O grafo criado foi:");
	for(int i = 0; i < a; i++) {
		printf("%d --(%d)--> %d\n", grafo->arestas[i].src, grafo->arestas[i].peso, grafo->arestas[i].dest);
	}
	Kruskal(grafo);
}

int main() {
	FILE *fp;
	int i;
	tGrafo* grafo;
	puts("*****ALGORITMO DE KRUSKAL*****");

	while(1) {
		printf("Selecione um arquivo de teste:\n1-)dij10.txt\n2-)dij20.txt\n3-)dij40.txt\n4-)dij50.txt\n5-)Sair\n");
		scanf("%d", &i);

		switch(i) {
			case 1:
				fp = fopen("test_files/dij10.txt", "r");
				break;
			case 2:
				fp = fopen("test_files/dij20.txt", "r");
				break;
			case 3:
				fp = fopen("test_files/dij40.txt", "r");
				break;
			case 4:
				fp = fopen("test_files/dij50.txt", "r");
				break;
			case 5:
				return 0;
			default:
				puts("Entrada invalida.");
				break;
		}	

		fileToGrafo(grafo, fp);
	}
}
