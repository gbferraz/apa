/************************************
 *       ALGORITMO DE DIJKSTRA      *
 *   Feito em C para a cadeira de   *
 *  Análise e Projeto de Algoritmos *
 *         Prof. Bruno Bruck        *
 *                                  *
 * CI-UFPB Feito por Gabriel Ferraz *
 ************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>

int dist_minima(int dist[], bool conjunto_SPT[], int num_vertices) {
	int min_index, min = INT_MAX;

	for(int i = 0; i < num_vertices; i++) {
		if(conjunto_SPT[i] == false && dist[i] <= min) {
			min = dist[i];
			min_index = i;
		}
	}

	return min_index;
}

void Prim(int **grafo, int num_vertices) {
	int dist[num_vertices];
	int origem = 0, destino = num_vertices - 1;
	bool conjunto_SPT[num_vertices];

	for(int i = 0; i < num_vertices; i++) {
		dist[i] = INT_MAX;
		conjunto_SPT[i] = false;
	}

	dist[origem] = 0;

	for(int i = 0; i < num_vertices-1; i++) {
		int u = dist_minima(dist, conjunto_SPT, num_vertices);
		conjunto_SPT[u] = true;

		for(int j = 0; j < num_vertices; j++) {
			if(!conjunto_SPT[j] && grafo[u][j] > 0 && dist[u] != INT_MAX && dist[u]+grafo[u][j] < dist[j]) {
				dist[j] = dist[u] + grafo[u][j];
			}
		}
		if(i == destino) break;
	}

	puts("Distancia minima encontrada:");
	printf("%d --(%d)--> %d\n", origem, dist[destino], destino);
}

void fileToGrafo(FILE *f) {
	int v;
	int **grafo;
	fscanf(f, "%d", &v);

	grafo = (int **)malloc(v * sizeof(int *));
	for(int i = 0; i < v; i++) grafo[i] = (int *)malloc(v * sizeof(int));

	int offset = 1, index = 0;
	for(int i = 0; i < v; i++) {
		for(int j = offset; j < v; j++) {
			fscanf(f, "%d", &(grafo[i][j]));
			index++;
		}
		offset++;
	}
	puts("");

	puts("O grafo criado foi:");
	for(int i = 0; i < v; i++) {
		for(int j = 0; j < v; j++) {
			if(grafo[i][j] != 0)
				printf("%d --(%d)--> %d\n", i, grafo[i][j], j);
		}
	}
	Prim(grafo, v);
	free(grafo);
}

int main() {
	FILE *fp;
	int i;
	puts("*****ALGORITMO DE DIJKSTRA*****");

	while(1) {
		printf("Selecione um arquivo de teste:\n1-)dij10.txt\n2-)dij20.txt\n3-)dij40.txt\n4-)dij50.txt\n5-)Sair\n");
		scanf("%d", &i);

		switch(i) {
			case 1:
				fp = fopen("test_files/dij10.txt", "r");
				break;
			case 2:
				fp = fopen("test_files/dij20.txt", "r");
				break;
			case 3:
				fp = fopen("test_files/dij40.txt", "r");
				break;
			case 4:
				fp = fopen("test_files/dij50.txt", "r");
				break;
			case 5:
				return 0;
			default:
				puts("Entrada invalida.");
				break;
		}	

		fileToGrafo(fp);
	}
}
