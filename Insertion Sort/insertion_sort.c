/****************************************************************************
 *                                                                          *
 *      Programa feito para comparar diferentes casos do Insertion Sort     *
 *  Desenvolvido para a cadeira de Análise e Projeto de Algoritmos - UFPB   *
 *                  Lencionada pelo Prof. Bruno Petrato                     *
 *                                                                          *
 ****************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<time.h>

//Define o tamanho do array
#define TAMANHO 40

//Função para exibir todos os elementos do array
void PrintArray(int A[], int n) {
    for(int i = 0; i < n; i++) {
        printf("%d ", A[i]);
    }
    
    printf("\n");
}

//Implementação do algoritmo do Insertion Sort em C
int InsertionSort(int A[], int n) {
    int pivo, j, i, iteracoes = 1;
    
    for(i = 1; i <= n-1; i++) {
        iteracoes++;
        pivo = A[i];
        j = i - 1;
        
        while(j >= 0 && A[j] > pivo) {
            iteracoes++;            
            A[j+1] = A[j];
            j = j - 1;
        }
        
        A[j+1] = pivo;

    }
    
    return iteracoes;
}

//Preenche o array de forma já ordenada que é o melhor caso para o insertion sort
void MelhorCaso(int A[], int n) {
    for(int i = 0; i < n; i++) {
        A[i] = i;
    }
}

//Preenche o array ordenado ao contrário que é o pior caso para o insertion sort
void PiorCaso(int A[], int n) {
    int j = n;
    for(int i = 0; i < n; i++) {
        A[i] = j - i;
    }
}

//Preenche o array com valores aleatórios
void CasoAleatorio(int A[], int n) {
    for(int i = 0; i < n; i++) {
        A[i] = rand() % n;
    }
}

int main() {
    int opcao, iteracoes, elementos[TAMANHO];
    //As variáveis do tipo clock_t salvam o valor do relógio do CPU
    //isso será usado para calcular o tempo de execução do algoritmo
    //O resultado é retornado em ticks de clock por segundo
    clock_t start, end;
    
    while(1) {
        printf("Escolha uma das opções:\n 1-) Melhor caso.\n 2-) Pior caso.\n 3-) Caso aleatório.\n 0-) Encerrar o programa\n");
        scanf("%d", &opcao);
        if(opcao == 0) break;
        puts("");
        
        switch(opcao) {
            case 1:
                MelhorCaso(elementos, TAMANHO);
                puts("O melhor caso é:");
                PrintArray(elementos, TAMANHO);
                
                start = clock();
                iteracoes = InsertionSort(elementos, TAMANHO);
                end = clock();
                
                puts("Resultado final:");
                PrintArray(elementos, TAMANHO);
                printf("Tempo gasto para organizar o array: %f ticks/s\nPassos Necessários: %d\n\n", (double)(end - start) / CLOCKS_PER_SEC, iteracoes);
                break;
                
            case 2:
                PiorCaso(elementos, TAMANHO);
                puts("O pior caso é:");
                PrintArray(elementos, TAMANHO);
                
                start = clock();
                iteracoes = InsertionSort(elementos, TAMANHO);
                end = clock();
                
                puts("Resultado final:");
                PrintArray(elementos, TAMANHO);
                printf("Tempo gasto para organizar o array: %f ticks/s\nPassos Necessários: %d\n\n", (double)(end - start) / CLOCKS_PER_SEC, iteracoes);
                break;
            
            case 3:
                CasoAleatorio(elementos, TAMANHO);
                puts("O array gerado foi:");
                PrintArray(elementos, TAMANHO);
                
                start = clock();
                iteracoes = InsertionSort(elementos, TAMANHO);
                end = clock();
                
                puts("Resultado final:");
                PrintArray(elementos, TAMANHO);
                printf("Tempo gasto para organizar o array: %f ticks/s\nPassos Necessários: %d\n\n", (double)(end - start) / CLOCKS_PER_SEC, iteracoes); 
                break;
                
            default:
                puts("Entrada inválida.");
                break;
        }
    }
    
    return 0;
}
