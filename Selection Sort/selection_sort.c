/****************************************************************************
 *                                                                          *
 *      Programa feito para comparar diferentes casos do Selection Sort     *
 *  Desenvolvido para a cadeira de Análise e Projeto de Algoritmos - UFPB   *
 *                  Lencionada pelo Prof. Bruno Petrato                     *
 *                                                                          *
 ****************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<time.h>

//Define o tamanho do array
#define TAMANHO 40

//Função para exibir todos os elementos do array
void PrintArray(int A[], int n) {
    for(int i = 0; i < n; i++) {
        printf("%d ", A[i]);
    }
    
    printf("\n");
}

//Implementação do algoritmo do Selection Sort em C
int SelectionSort(int A[], int n) {
    int i_min, j, i, iteracoes = 1;
    
    for(i = 0; i <= n-1; i++) {
        iteracoes++;
        i_min = i;
        
        for(j = i + 1; j <= n-1; j++) {
            iteracoes++;
            if(A[j] < A[i_min]) i_min = j;
        }
        
        if(A[i] != A[i_min]) {
            int temp = A[i];
            A[i] = A[i_min];
            A[i_min] = temp;
        }
    }
    
    return iteracoes;
}

//Preenche o array em ordem crescente
void CasoCrescente(int A[], int n) {
    for(int i = 0; i < n; i++) {
        A[i] = i;
    }
}

//Preenche o array em ordem decrescente
void CasoDecrescente(int A[], int n) {
    int j = n;
    for(int i = 0; i < n; i++) {
        A[i] = j - i;
    }
}

//Preenche o array com valores aleatórios
void CasoAleatorio(int A[], int n) {
    for(int i = 0; i < n; i++) {
        A[i] = rand() % n;
    }
}

int main() {
    int opcao, iteracoes, elementos[TAMANHO];
    //As variáveis do tipo clock_t salvam o valor do relógio do CPU
    //isso será usado para calcular o tempo de execução do algoritmo
    //O resultado é retornado em ticks de clock por segundo
    clock_t start, end;
    
    while(1) {
        printf("Escolha uma das opções:\n 1-) Caso crescente.\n 2-) Caso decrescente.\n 3-) Caso aleatório.\n 0-) Encerrar o programa\n");
        scanf("%d", &opcao);
        if(opcao == 0) break;
        puts("");
        
        switch(opcao) {
            case 1:
                CasoCrescente(elementos, TAMANHO);
                puts("Em ordem crescente:");
                PrintArray(elementos, TAMANHO);
                
                start = clock();
                iteracoes = SelectionSort(elementos, TAMANHO);
                end = clock();
                
                puts("Resultado final:");
                PrintArray(elementos, TAMANHO);
                printf("Tempo gasto para organizar o array: %f ticks/s\nPassos Necessários: %d\n\n", (double)(end - start) / CLOCKS_PER_SEC, iteracoes);
                break;
                
            case 2:
                CasoDecrescente(elementos, TAMANHO);
                puts("Em ordem decrescente:");
                PrintArray(elementos, TAMANHO);
                
                start = clock();
                iteracoes = SelectionSort(elementos, TAMANHO);
                end = clock();
                
                puts("Resultado final:");
                PrintArray(elementos, TAMANHO);
                printf("Tempo gasto para organizar o array: %f ticks/s\nPassos Necessários: %d\n\n", (double)(end - start) / CLOCKS_PER_SEC, iteracoes);
                break;
            
            case 3:
                CasoAleatorio(elementos, TAMANHO);
                puts("O array gerado foi:");
                PrintArray(elementos, TAMANHO);
                
                start = clock();
                iteracoes = SelectionSort(elementos, TAMANHO);
                end = clock();
                
                puts("Resultado final:");
                PrintArray(elementos, TAMANHO);
                printf("Tempo gasto para organizar o array: %f ticks/s\nPassos Necessários: %d\n\n", (double)(end - start) / CLOCKS_PER_SEC, iteracoes); 
                break;
                
            default:
                puts("Entrada inválida.");
                break;
        }
    }
    
    return 0;
}
